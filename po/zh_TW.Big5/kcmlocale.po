# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: kcmlocale\n"
"POT-Creation-Date: 1998-05-13 12:05+0200\n"
"PO-Revision-Date: 1998-04-20 21:47+0800\n"
"Last-Translator: Chou Yeh-Jyi <ycchou@ccca.nctu.edu.tw>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING\n"

#: main.cpp:52
msgid "&Locale"
msgstr "地區[&L]"

#: main.cpp:58
msgid "usage: kcmlocale [-init | language]\n"
msgstr "用法: kcmlocale [-init | language]\n"

#: main.cpp:86
msgid "Locale settings"
msgstr "地區設定"

#: locale.cpp:45
msgid "Language"
msgstr "語系"

#: locale.cpp:55
msgid "First"
msgstr "第一選擇"

#: locale.cpp:65
msgid "Second"
msgstr "第二選擇"

#: locale.cpp:75
msgid "Third"
msgstr "第三選擇"

#: locale.cpp:115
msgid "without name!"
msgstr "沒有名稱!"

#: locale.cpp:181
msgid "Applying language settings"
msgstr "套用語系設定"

#: locale.cpp:182
msgid ""
"Changed language settings apply only to newly started applications.\n"
"To change the language of all programs, you will have to logout first."
msgstr ""
"改變語系設定只會影響到新開啟的應用程式.\n"
"要改變所有程式的語系，必須先 logout."
